package client;

import cz.cvut.fit.klickjan.tjv_sem.pl.dto.AnimalModel;
import cz.cvut.fit.klickjan.tjv_sem.pl.dto.CaretakerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.HypermediaRestTemplateConfigurer;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class Client implements CommandLineRunner {
    @Autowired
    private AnimalResource animalResource;
    @Autowired
    private CaretakerResource caretakerResource;

    @Bean
    RestTemplateCustomizer restTemplateCustomizer(HypermediaRestTemplateConfigurer configurer) {
        return configurer::registerHypermediaTypes;
    }

    public static void main(String[] args) {
        System.out.println("Henlo fren");
        SpringApplication.run(Client.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String command = args[0];
        List<String> arguments = List.of(args);
        switch (command) {
            case "create": {
                System.out.println(animalResource.create(new AnimalModel(args[1], 10., List.of(), List.of())));
                break;
            }

            case "update": {
                long id = Integer.parseInt(args[1]);
                AnimalModel model = animalResource.getOne(id);
                model.setName(args[2]);
                model.setCaretakers(arguments.subList(3, arguments.size()).stream().map(Long::valueOf).collect(Collectors.toList()));
                animalResource.update(model);
                System.out.println(animalResource.getOne(id));
                break;
            }
            case "get": {
                System.out.println(animalResource.getOne(Integer.parseInt(args[1])));
                break;
            }
            case "get-all": {
                System.out.println(animalResource.getAll());
                break;
            }
            case "filter": {
                System.out.println(animalResource.filter(List.of(args).subList(1, args.length).stream().map(Long::valueOf).collect(Collectors.toList())));
                break;
            }
            case "caretakers": {
                CaretakerModel one = new CaretakerModel("C1");
                CaretakerModel two = new CaretakerModel("C2");
                CaretakerModel three = new CaretakerModel("C3");
                System.out.println(caretakerResource.create(one));
                System.out.println(caretakerResource.create(two));
                System.out.println(caretakerResource.create(three));
                break;
            }
            case "animals": {
                System.out.println(animalResource.create(new AnimalModel(args[1], 10., List.of(1L), List.of())));
                System.out.println(animalResource.create(new AnimalModel(args[2], 10., List.of(1L, 2L), List.of())));
                System.out.println(animalResource.create(new AnimalModel(args[3], 10., List.of(2L, 3L), List.of())));
                System.out.println(animalResource.create(new AnimalModel(args[4], 10., List.of(3L), List.of())));
                break;
            }
            case "run-all": {
                AnimalModel animal = animalResource.create(new AnimalModel("New animal", 10., List.of(), List.of()));
                System.out.println(animal);
                animal.setName("New name");
                animalResource.update(animal);
                AnimalModel get = animalResource.getOne(animal.getId());
                System.out.println(get);
                System.out.println(animalResource.getAll());
                animalResource.delete(get);
                System.out.println(animalResource.getAll());
                break;
            }
        }
    }
}
