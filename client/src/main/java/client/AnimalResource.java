package client;


import cz.cvut.fit.klickjan.tjv_sem.pl.dto.AnimalModel;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Component
public class AnimalResource {
    private final RestTemplate restTemplate;

    private static final String URI_TEMPLATE = "http://localhost:8080/animal";

    public AnimalResource(RestTemplateBuilder builder) {
        restTemplate = builder.build();
    }

    public List<AnimalModel> getAll() {
        return restTemplate.exchange(URI_TEMPLATE, HttpMethod.GET, null, new ParameterizedTypeReference<List<AnimalModel>>() {}).getBody();
    }

    public AnimalModel getOne(long id) {
        return restTemplate.getForObject(URI_TEMPLATE + "/" + id, AnimalModel.class);
    }

    public AnimalModel create(AnimalModel model) {
        return restTemplate.postForObject(URI_TEMPLATE, model, AnimalModel.class);
    }

    public void update(AnimalModel model) {
        restTemplate.put(URI_TEMPLATE + "/" + model.getId(), model);
    }

    public void delete(AnimalModel model) {
        restTemplate.delete(URI_TEMPLATE + "/" + model.getId());
    }

    public List<AnimalModel> filter(List<Long> ids) {
        UriComponents uri = UriComponentsBuilder.fromHttpUrl(URI_TEMPLATE)
                .queryParam("caretakerId", ids)
                .build();
        return restTemplate.exchange(uri.toUri(), HttpMethod.GET, null, new ParameterizedTypeReference<List<AnimalModel>>() {}).getBody();
    }
}
