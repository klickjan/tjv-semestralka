package client;

import cz.cvut.fit.klickjan.tjv_sem.pl.dto.CaretakerModel;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CaretakerResource {
    private final RestTemplate restTemplate;

    private static final String URI_TEMPLATE = "http://localhost:8080/caretaker";

    public CaretakerResource(RestTemplateBuilder builder) {
        restTemplate = builder.build();
    }

    public CaretakerModel create(CaretakerModel model) {
        return restTemplate.postForObject(URI_TEMPLATE, model, CaretakerModel.class);
    }
}
