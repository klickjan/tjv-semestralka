package cz.cvut.fit.klickjan.tjv_sem.pl.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Set;

public class SocialMediaPostModel extends RepresentationModel<SocialMediaPostModel> {
    private long id;
    private int likes;
    private Set<Long> caretakerIds;
    private Set<Long> animalIds;

    public SocialMediaPostModel() {
    }

    public SocialMediaPostModel(long id, int likes, Set<Long> caretakerIds, Set<Long> animalIds) {
        this.id = id;
        this.likes = likes;
        this.caretakerIds = caretakerIds;
        this.animalIds = animalIds;
    }

    public long getId() {
        return id;
    }

    public int getLikes() {
        return likes;
    }

    public Set<Long> getCaretakerIds() {
        return caretakerIds;
    }

    public Set<Long> getAnimalIds() {
        return animalIds;
    }

    @Override
    public String toString() {
        return "SocialMediaPostModel{" +
                "id=" + id +
                ", likes=" + likes +
                ", caretakerIds=" + caretakerIds +
                ", animalIds=" + animalIds +
                '}';
    }
}
