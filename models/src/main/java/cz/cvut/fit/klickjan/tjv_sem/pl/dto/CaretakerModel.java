package cz.cvut.fit.klickjan.tjv_sem.pl.dto;

import org.springframework.hateoas.RepresentationModel;

public class CaretakerModel extends RepresentationModel<CaretakerModel> {
    private long id;
    private String name;

    public CaretakerModel() {
    }

    public CaretakerModel(String name) {
        this.id = 0L;
        this.name = name;
    }

    public CaretakerModel(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "CaretakerModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
