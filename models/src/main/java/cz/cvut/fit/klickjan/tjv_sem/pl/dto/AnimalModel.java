package cz.cvut.fit.klickjan.tjv_sem.pl.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class AnimalModel extends RepresentationModel<AnimalModel> {
    private long id;
    private String name;
    private double weight;
    private List<Long> caretakers;
    private List<Long> favouritedCaretakers;

    public AnimalModel() {
    }

    public AnimalModel(String name, double weight, List<Long> caretakers, List<Long> favouritedCaretakers) {
        this.id = 0;
        this.name = name;
        this.weight = weight;
        this.caretakers = caretakers;
        this.favouritedCaretakers = favouritedCaretakers;
    }

    public AnimalModel(long id, String name, double weight, List<Long> caretakers, List<Long> favouritedCaretakers) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.caretakers = caretakers;
        this.favouritedCaretakers = favouritedCaretakers;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public List<Long> getCaretakers() {
        return caretakers;
    }

    public List<Long> getFavouritedCaretakers() {
        return favouritedCaretakers;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setCaretakers(List<Long> caretakers) {
        this.caretakers = caretakers;
    }

    public void setFavouritedCaretakers(List<Long> favouritedCaretakers) {
        this.favouritedCaretakers = favouritedCaretakers;
    }

    @Override
    public String toString() {
        return "AnimalModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", caretakers=" + caretakers +
                ", favouritedCaretakers=" + favouritedCaretakers +
                '}';
    }
}
