FROM openjdk:13-jdk AS app-build

ENV GRADLE_OPTS -Dorg.gradle.daemon=false

COPY . /build
WORKDIR /build
RUN chmod +x gradlew
RUN ./gradlew test
