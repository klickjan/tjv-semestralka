package cz.cvut.fit.klickjan.tjv_sem;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootTest
@AutoConfigureMockMvc
@EnableJpaRepositories({"cz.cvut.fit.klickjan.tjv_sem.dl.repository"})
@EntityScan({"cz.cvut.fit.klickjan.tjv_sem.dl.dm"})
public class SpringTest {
}
