package cz.cvut.fit.klickjan.tjv_sem.bl;

import cz.cvut.fit.klickjan.tjv_sem.SpringTest;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.AnimalRepository;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.CaretakerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.mockito.BDDMockito.given;
import static cz.cvut.fit.klickjan.tjv_sem.dl.dm.TestData.*;


class AnimalServiceTest extends SpringTest {
    @Autowired
    private AnimalService animalService;

    @MockBean
    private AnimalRepository animalRepository;
    @MockBean
    private CaretakerRepository caretakerRepository;

    @Test
    void findByCaretaker() {
        given(animalRepository.findAnimalByCaretakersIdIn(List.of(1L))).willReturn(List.of(bear, tiger));
        Assert.assertEquals(List.of(bear, tiger), animalService.findByCaretakers(List.of(1L)));
    }

    @Test
    void findByCaretakers() {
        given(animalRepository.findAnimalByCaretakersIdIn(List.of(2L, 3L))).willReturn(List.of(tiger, piggy));
        Assert.assertEquals(List.of(tiger, piggy), animalService.findByCaretakers(List.of(2L, 3L)));
    }

    @Test
    void findAll() {
        given(animalRepository.findAll()).willReturn(List.of(bear, tiger, piggy));
        Assert.assertEquals(List.of(bear, tiger, piggy), animalService.findAll());
    }

    @Test
    void save() {
        given(animalRepository.save(bear)).willReturn(bear);
        Assert.assertEquals(bear, animalService.save(bear));

        Animal noIdAnimal = new Animal("no", 1, Set.of(), List.of());
        Animal idAnimal = new Animal(4L, "no", 1, Set.of(), List.of());
        given(animalRepository.save(noIdAnimal)).willReturn(idAnimal);
        Assert.assertEquals(idAnimal, animalService.save(noIdAnimal));
    }

    @Test()
    void saveFail() {
        Animal underWeight = new Animal("underWeight", -1, Set.of(), List.of());
        IllegalArgumentException e = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> animalService.save(underWeight)
        );

        Animal caretakerMismatch = new Animal("caretakerMismatch", 10, Set.of(caretaker1, caretaker2), List.of(caretaker2, caretaker3));
        IllegalArgumentException e2 = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> animalService.save(underWeight)
        );
    }

    @Test
    void findById() {
    }
}