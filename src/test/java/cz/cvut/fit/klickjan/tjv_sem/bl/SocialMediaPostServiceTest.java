package cz.cvut.fit.klickjan.tjv_sem.bl;

import cz.cvut.fit.klickjan.tjv_sem.SpringTest;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.SocialMediaPost;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.TestData;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.SocialMediaPostRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Set;

import static cz.cvut.fit.klickjan.tjv_sem.dl.dm.TestData.*;
import static org.mockito.BDDMockito.given;

class SocialMediaPostServiceTest extends SpringTest {
    @MockBean
    private AnimalService animalService;
    @MockBean
    private CaretakerService caretakerService;

    @MockBean
    private SocialMediaPostRepository socialMediaPostRepository;

    private final SocialMediaPostService socialMediaPostService;

    @Autowired
    public SocialMediaPostServiceTest(SocialMediaPostService socialMediaPostService) {
        this.socialMediaPostService = socialMediaPostService;
    }

    @Test
    void find() {
        SocialMediaPost one = new SocialMediaPost(1, Set.of(caretaker1), Set.of(tiger), 10);
        SocialMediaPost two = new SocialMediaPost(2, Set.of(caretaker1, caretaker2, caretaker3), Set.of(bear, tiger), 15);

        given(animalService.findById(List.of(1L))).willReturn(List.of(TestData.bear));
        given(caretakerService.findAllById(List.of(1L, 2L))).willReturn(List.of(caretaker1, caretaker2));
        given(socialMediaPostRepository.findByAnimalsIdInAndCaretakersIdIn(Set.of(1L), Set.of(2L))).willReturn(List.of(two));
        Assert.assertEquals(List.of(two), socialMediaPostService.findById(List.of(1L), List.of(2L)));
    }
}