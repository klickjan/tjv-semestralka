package cz.cvut.fit.klickjan.tjv_sem.dl.dm;

import java.util.List;
import java.util.Set;

public class TestData {
    public static final Caretaker caretaker1 = new Caretaker(1L, "Caretaker 1");
    public static final Caretaker caretaker2 = new Caretaker(2L, "Caretaker 2");
    public static final Caretaker caretaker3 = new Caretaker(3L, "Caretaker 3");

    public static final Animal bear = new Animal(1L, "Medivedk Pu", 12, Set.of(caretaker1), List.of(caretaker1));
    public static final Animal tiger = new Animal(2L, "Tigrik", 12, Set.of(caretaker1, caretaker2), List.of(caretaker2));
    public static final Animal piggy = new Animal(3L, "Prasatko", 12, Set.of(caretaker2, caretaker3), List.of(caretaker3));

}
