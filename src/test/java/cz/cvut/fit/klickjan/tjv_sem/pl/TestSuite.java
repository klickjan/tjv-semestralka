package cz.cvut.fit.klickjan.tjv_sem.pl;

import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.SocialMediaPost;

import java.util.List;
import java.util.Set;

public class TestSuite {
    public static final List<Caretaker> caretakers = List.of(
            new Caretaker(1L, "C1"),
            new Caretaker(2L, "C2"),
            new Caretaker(3L, "C3")
    );

    public static final List<Animal> animals = List.of(
            new Animal(1L, "An1", 1, Set.of(caretakers.get(0)), List.of()),
            new Animal(2L, "An2", 1, Set.of(caretakers.get(1), caretakers.get(2)), List.of()),
            new Animal(3L, "An3", 1, Set.of(caretakers.get(1)), List.of()),
            new Animal(4L, "An4", 1, Set.of(caretakers.get(0), caretakers.get(2)), List.of())
    );

    public static final List<SocialMediaPost> posts = List.of(
            new SocialMediaPost(1L, Set.of(caretakers.get(0), caretakers.get(1)), Set.of(animals.get(0)), 10),
            new SocialMediaPost(2L, Set.of(caretakers.get(1), caretakers.get(2)), Set.of(animals.get(1), animals.get(2)), 32),
            new SocialMediaPost(3L, Set.of(caretakers.get(2)), Set.of(animals.get(3), animals.get(2)), 24)
    );
}
