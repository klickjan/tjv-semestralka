package cz.cvut.fit.klickjan.tjv_sem.pl;

import cz.cvut.fit.klickjan.tjv_sem.SpringTest;
import cz.cvut.fit.klickjan.tjv_sem.bl.SocialMediaPostService;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.SocialMediaPost;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

import static cz.cvut.fit.klickjan.tjv_sem.pl.TestSuite.animals;
import static cz.cvut.fit.klickjan.tjv_sem.pl.TestSuite.posts;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;

class SocialMediaPostControllerTest extends SpringTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SocialMediaPostService socialMediaPostService;


    @Test
    void get() throws Exception {
        BDDMockito.given(socialMediaPostService.findById(1L)).willReturn(Optional.of(posts.get(0)));
        var res = mockMvc.perform(
                MockMvcRequestBuilders.get("/social-media-post/1")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.likes", CoreMatchers.is(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/social-media-post/1")));
    }

    @Test
    void save() throws Exception {
        BDDMockito.given(socialMediaPostService.save(any(SocialMediaPost.class))).willReturn(posts.get(1));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/social-media-post")
                        .contentType("application/json")
                        .content("{\"likes\" : \"32\", \"caretakerIds\": [], \"animalIds\":[]}")
        ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.likes", CoreMatchers.is(32)))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/social-media-post/2")));
    }

    @Test
    void update() throws Exception{
        BDDMockito.given(socialMediaPostService.save(any(SocialMediaPost.class))).willReturn(posts.get(2));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/social-media-post/3")
                        .contentType("application/json")
                        .content("{\"likes\" : \"24\", \"caretakerIds\": [], \"animalIds\":[]}")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.likes", CoreMatchers.is(24)))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/social-media-post/3")));
    }

    @Test
    void find()throws Exception {
        BDDMockito.given(socialMediaPostService.findById(anyList(), anyList())).willReturn(List.of(posts.get(0), posts.get(1)));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/social-media-post")
                        .param("caretakerId", "1", "2")
                        .param("animalId", "1", "2")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].likes", CoreMatchers.is(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].likes", CoreMatchers.is(32)));
    }
}