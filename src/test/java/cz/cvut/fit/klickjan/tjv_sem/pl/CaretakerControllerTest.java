package cz.cvut.fit.klickjan.tjv_sem.pl;

import cz.cvut.fit.klickjan.tjv_sem.SpringTest;
import cz.cvut.fit.klickjan.tjv_sem.bl.CaretakerService;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static cz.cvut.fit.klickjan.tjv_sem.pl.TestSuite.caretakers;

class CaretakerControllerTest extends SpringTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CaretakerService caretakerService;

    @Test
    void getOne() throws Exception {
        BDDMockito.given(caretakerService.findById(1L)).willReturn(Optional.of(caretakers.get(0)));
        var res = mockMvc.perform(
                MockMvcRequestBuilders.get("/caretaker/1")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is("C1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/caretaker/1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.taking-care-of.href", CoreMatchers.endsWith("/animal?caretakerId=1")));
    }

    @Test
    void create() throws Exception {
        BDDMockito.given(caretakerService.save(ArgumentMatchers.any(Caretaker.class))).willReturn(caretakers.get(1));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/caretaker")
                        .contentType("application/json")
                        .content("{\"name\" : \"C2\"}")
        ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is("C2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/caretaker/2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.taking-care-of.href", CoreMatchers.endsWith("/animal?caretakerId=2")));

    }

    @Test
    void update() throws Exception {
        BDDMockito.given(caretakerService.save(ArgumentMatchers.any(Caretaker.class))).willReturn(caretakers.get(2));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .put("/caretaker/3")
                        .contentType("application/json")
                        .content("{\"name\" : \"C3\"}")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is("C3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/caretaker/3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.taking-care-of.href", CoreMatchers.endsWith("/animal?caretakerId=3")));
    }
}