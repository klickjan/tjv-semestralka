package cz.cvut.fit.klickjan.tjv_sem.pl;

import cz.cvut.fit.klickjan.tjv_sem.SpringTest;
import cz.cvut.fit.klickjan.tjv_sem.bl.AnimalService;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

import static cz.cvut.fit.klickjan.tjv_sem.pl.TestSuite.animals;

class AnimalControllerTest extends SpringTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AnimalService animalService;

    @Test
    void all() throws Exception {
        BDDMockito.given(animalService.findAll()).willReturn(animals);
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/animal")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name", CoreMatchers.is("An1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].name", CoreMatchers.is("An2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[2].name", CoreMatchers.is("An3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[3].name", CoreMatchers.is("An4")));
    }

    @Test
    void getOne() throws Exception {
        BDDMockito.given(animalService.findById(1L)).willReturn(Optional.of(animals.get(0)));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/animal/1")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is("An1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/animal/1")));
    }

    @Test
    void byCaretakers() throws Exception {
        BDDMockito.given(animalService.findByCaretakers(List.of(1L, 3L))).willReturn(List.of(animals.get(0), animals.get(1), animals.get(3)));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .get("/animal")
                        .param("caretakerId", "1", "3")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].name", CoreMatchers.is("An1")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].name", CoreMatchers.is("An2")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[2].name", CoreMatchers.is("An4")));
    }

    @Test
    void create() throws Exception {
        BDDMockito.given(animalService.save(ArgumentMatchers.any(Animal.class))).willReturn(animals.get(2));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/animal")
                        .contentType("application/json")
                        .content("{\"name\" : \"An3\", \"weight\":1, \"caretakers\": [1], \"favouritedCaretakers\":[]}")
        ).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is("An3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/animal/3")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.social_media_posts.href", CoreMatchers.allOf(
                        CoreMatchers.containsString("social-media-post?"),
                        CoreMatchers.containsString("animalId=3"),
                        CoreMatchers.containsString("caretakerId=2")
                )));
    }

    @Test
    void update() throws Exception {
        BDDMockito.given(animalService.save(ArgumentMatchers.any(Animal.class))).willReturn(animals.get(3));
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/animal")
                        .contentType("application/json")
                        .content("{\"name\" : \"An4\", \"weight\":1, \"caretakers\": [1], \"favouritedCaretakers\":[]}")
        ).andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is("An4")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.self.href", CoreMatchers.endsWith("/animal/4")))
                .andExpect(MockMvcResultMatchers.jsonPath("$._links.social_media_posts.href",
                        CoreMatchers.allOf(
                                CoreMatchers.containsString("social-media-post?"),
                                CoreMatchers.containsString("animalId=4"),
                                CoreMatchers.containsString("caretakerId=3"),
                                CoreMatchers.containsString("caretakerId=1")
                        )));

    }

    @Test
    void delete() throws Exception {
        BDDMockito.willDoNothing().given(animalService).delete(1L);
        var res = mockMvc.perform(
                MockMvcRequestBuilders
                        .delete("/animal/1")
        ).andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}