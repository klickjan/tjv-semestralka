package cz.cvut.fit.klickjan.tjv_sem.bl;

import cz.cvut.fit.klickjan.tjv_sem.SpringTest;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.CaretakerRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

class CaretakerServiceTest extends SpringTest {
    @Autowired
    private CaretakerService caretakerService;

    @MockBean
    private CaretakerRepository caretakerRepository;

    private final Caretaker caretaker1 = new Caretaker(1L, "c1");
    private final Caretaker caretaker1NoId = new Caretaker("c1_noid");
    private final Caretaker caretaker2 = new Caretaker(2L, "c2");
    private final Caretaker caretaker3 = new Caretaker(3L, "c3");

    @Test
    void save() {
        given(caretakerRepository.save(caretaker1NoId)).willReturn(caretaker1);
        Assert.assertEquals(caretaker1, caretakerService.save(caretaker1NoId));
    }

    @Test
    void delete() {
        given(caretakerRepository.existsById(1L)).willReturn(true);
        caretakerService.delete(1L);
        given(caretakerRepository.existsById(1L)).willReturn(false);
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
                () -> caretakerService.delete(1L)
        );
    }

    @Test
    void findAllById() {
        given(caretakerRepository.findAllById(List.of(1L, 2L))).willReturn(List.of(caretaker1, caretaker2));
        Assert.assertEquals(List.of(caretaker1, caretaker2), caretakerService.findAllById(List.of(1L, 2L)));
    }

    @Test
    void findById() {
        given(caretakerRepository.findById(3L)).willReturn(Optional.of(caretaker3));
        Assert.assertEquals(caretaker3, caretakerService.findById(3L).get());
    }
}