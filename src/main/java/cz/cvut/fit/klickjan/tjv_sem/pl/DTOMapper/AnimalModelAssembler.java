package cz.cvut.fit.klickjan.tjv_sem.pl.DTOMapper;

import cz.cvut.fit.klickjan.tjv_sem.bl.AnimalService;
import cz.cvut.fit.klickjan.tjv_sem.bl.CaretakerService;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.BaseModel;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.pl.AnimalController;
import cz.cvut.fit.klickjan.tjv_sem.pl.SocialMediaPostController;
import cz.cvut.fit.klickjan.tjv_sem.pl.dto.AnimalModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class AnimalModelAssembler implements RepresentationModelAssembler<Animal, AnimalModel> {
    private CaretakerService caretakerService;
    private AnimalService animalService;

    @Autowired
    public AnimalModelAssembler(CaretakerService caretakerService, AnimalService animalService) {
        this.caretakerService = caretakerService;
        this.animalService = animalService;
    }

    @Override
    public AnimalModel toModel(Animal animal) {
        AnimalModel model = new AnimalModel(animal.getId(), animal.getName(), animal.getWeight(),
                BaseModel.mapToIds(animal.getCaretakers()),
                BaseModel.mapToIds(animal.getFavouritedCaretakers()));
        model.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(AnimalController.class).getOne(animal.getId())).withSelfRel());
        model.add(
                WebMvcLinkBuilder.linkTo(
                        WebMvcLinkBuilder.methodOn(SocialMediaPostController.class)
                                .find(List.of(animal.getId()), model.getCaretakers())
                ).withRel("social_media_posts")
        );
        return model;
    }

    public Animal fromModel(AnimalModel animalModel) {
        Set<Caretaker> caretakers = new HashSet<>(caretakerService.findAllById(animalModel.getCaretakers()));
        List<Caretaker> favouritedCaretakers = caretakerService.findAllById(animalModel.getFavouritedCaretakers());
        Animal animal;
        if (animalModel.getId() > 0) {
            animal = animalService.findById(animalModel.getId()).get();
        } else {
            animal = new Animal();
        }
        animal.setName(animalModel.getName());
        animal.setWeight(animalModel.getWeight());
        animal.setCaretakers(caretakers);
        animal.setFavouritedCaretakers(favouritedCaretakers);
        return animal;
    }
}
