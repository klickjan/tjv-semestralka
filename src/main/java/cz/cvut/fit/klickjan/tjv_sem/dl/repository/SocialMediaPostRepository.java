package cz.cvut.fit.klickjan.tjv_sem.dl.repository;

import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.SocialMediaPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface SocialMediaPostRepository extends JpaRepository<SocialMediaPost, Long> {
    List<SocialMediaPost> findByAnimalsIdInAndCaretakersIdIn(Set<Long> animalsId, Set<Long> caretakersId);
}
