package cz.cvut.fit.klickjan.tjv_sem.pl;

import cz.cvut.fit.klickjan.tjv_sem.bl.CaretakerService;
import cz.cvut.fit.klickjan.tjv_sem.pl.DTOMapper.CaretakerModelAssembler;
import cz.cvut.fit.klickjan.tjv_sem.pl.dto.CaretakerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/caretaker")
public class CaretakerController {

    private final CaretakerService caretakerService;
    private final CaretakerModelAssembler caretakerModelAssembler;

    @Autowired
    public CaretakerController(CaretakerService caretakerService, CaretakerModelAssembler caretakerModelAssembler) {
        this.caretakerService = caretakerService;
        this.caretakerModelAssembler = caretakerModelAssembler;
    }

    @GetMapping(path = "/{id}")
    public CaretakerModel getOne(@PathVariable("id") long id) {
        return caretakerModelAssembler.toModel(caretakerService.findById(id).get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CaretakerModel create(@RequestBody CaretakerModel caretakerModel) {
        return caretakerModelAssembler.toModel(caretakerService.save(caretakerModelAssembler.fromModel(caretakerModel)));
    }

    @PutMapping(path = "/{id}")
    public CaretakerModel update(@PathVariable("id") long id, @RequestBody CaretakerModel caretakerModel) {
        return caretakerModelAssembler.toModel(caretakerService.save(caretakerModelAssembler.fromModel(caretakerModel)));
    }
}
