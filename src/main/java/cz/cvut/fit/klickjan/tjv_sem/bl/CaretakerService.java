package cz.cvut.fit.klickjan.tjv_sem.bl;

import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.CaretakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CaretakerService {
    private final CaretakerRepository caretakerRepository;

    @Autowired
    public CaretakerService(CaretakerRepository caretakerRepository) {
        this.caretakerRepository = caretakerRepository;
    }

    public Caretaker save(Caretaker caretaker) {
        return caretakerRepository.save(caretaker);
    }

    public void delete(long id) {
        if (!caretakerRepository.existsById(id)) {
            throw new IllegalArgumentException("No such entity with id " + id + " exists");
        }
        caretakerRepository.deleteById(id);
    }

    public @NotNull List<Caretaker> findAllById(List<Long> caretakers) {
        return caretakerRepository.findAllById(caretakers);
    }

    public Optional<Caretaker> findById(long id) {
        return caretakerRepository.findById(id);
    }
}
