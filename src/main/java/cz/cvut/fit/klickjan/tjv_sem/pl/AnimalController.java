package cz.cvut.fit.klickjan.tjv_sem.pl;

import cz.cvut.fit.klickjan.tjv_sem.bl.AnimalService;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.pl.DTOMapper.AnimalModelAssembler;
import cz.cvut.fit.klickjan.tjv_sem.pl.dto.AnimalModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/animal")
public class AnimalController {
    private final AnimalService animalService;
    private final AnimalModelAssembler animalModelAssembler;

    @Autowired
    public AnimalController(AnimalService animalService, AnimalModelAssembler animalModelAssembler) {
        this.animalService = animalService;
        this.animalModelAssembler = animalModelAssembler;
    }

    @GetMapping
    public List<AnimalModel> all() {
        return animalService.findAll().stream()
                .map(animalModelAssembler::toModel)
                .collect(Collectors.toList());
    }

    @GetMapping(path = "/{id}")
    public AnimalModel getOne(@PathVariable("id") long id) {
        return animalModelAssembler.toModel(animalService.findById(id).get());
    }

    @GetMapping(params = {"caretakerId"})
    public List<AnimalModel> byCaretakers(@RequestParam("caretakerId") List<Long> caretakerIds) {
        return animalService.findByCaretakers(caretakerIds).stream()
                .map(animalModelAssembler::toModel)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AnimalModel create(@RequestBody AnimalModel animal) {
        Animal entity = animalModelAssembler.fromModel(animal);
        if (entity.getId() > 0) {
            throw new IllegalArgumentException("Id should be empty when creating!");
        }
        return animalModelAssembler.toModel(animalService.save(entity));
    }

    @PutMapping(path = "/{id}")
    public AnimalModel update(@PathVariable("id") long id, @RequestBody AnimalModel animal) {
        Animal entity = animalModelAssembler.fromModel(animal);
        entity.setId(id);
        return animalModelAssembler.toModel(animalService.save(entity));
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        animalService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
