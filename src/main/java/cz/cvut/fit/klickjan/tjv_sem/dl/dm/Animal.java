package cz.cvut.fit.klickjan.tjv_sem.dl.dm;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
public class Animal implements BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    private double weight;

    @ManyToMany
    @JoinTable(
            name = "animal_caretaker",
            joinColumns = @JoinColumn(name = "animal_id"),
            inverseJoinColumns = @JoinColumn(name = "caretaker_id")
    )
    private @NotNull Set<Caretaker> caretakers;

    @OneToMany()
    @JoinColumn(name = "favourite_animal_id")
    private @NotNull List<Caretaker> favouritedCaretakers;

    public Animal() {
    }

    public Animal(@NotNull String name, double weight, @NotNull Set<Caretaker> caretakers, @NotNull List<Caretaker> favouritedCaretakers) {
        this.name = name;
        this.weight = weight;
        this.caretakers = caretakers;
        this.favouritedCaretakers = favouritedCaretakers;
    }

    public Animal(long id, @NotNull String name, double weight, @NotNull Set<Caretaker> caretakers, @NotNull List<Caretaker> favouritedCaretakers) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.caretakers = caretakers;
        this.favouritedCaretakers = favouritedCaretakers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public @NotNull Set<Caretaker> getCaretakers() {
        return caretakers;
    }

    public void setCaretakers(@NotNull Set<Caretaker> careTakers) {
        this.caretakers = careTakers;
    }

    public @NotNull List<Caretaker> getFavouritedCaretakers() {
        return favouritedCaretakers;
    }

    public void setFavouritedCaretakers(@NotNull List<Caretaker> favouritedCareTakers) {
        this.favouritedCaretakers = favouritedCareTakers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Objects.equals(getId(), animal.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
