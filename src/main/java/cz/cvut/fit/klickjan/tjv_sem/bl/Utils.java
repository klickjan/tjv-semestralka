package cz.cvut.fit.klickjan.tjv_sem.bl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {
    public static <T> Set<T> toSet(List<T> a){
        return new HashSet<>(a);
    }
}
