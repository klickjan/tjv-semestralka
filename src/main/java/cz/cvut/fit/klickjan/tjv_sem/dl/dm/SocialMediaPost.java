package cz.cvut.fit.klickjan.tjv_sem.dl.dm;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class SocialMediaPost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToMany
    @JoinTable(
            name = "social_media_post_caretaker",
            joinColumns = @JoinColumn(name = "social_media_post_id"),
            inverseJoinColumns = @JoinColumn(name = "caretaker_id")
    )
    private Set<Caretaker> caretakers;

    @ManyToMany
    @JoinTable(
            name = "social_media_post_animal",
            joinColumns = @JoinColumn(name = "social_media_post_id"),
            inverseJoinColumns = @JoinColumn(name = "animal_id")
    )
    private Set<Animal> animals;

    private int likes;

    public SocialMediaPost() {
    }

    public SocialMediaPost(Set<Caretaker> caretakers, Set<Animal> animals, int likes) {
        this.caretakers = caretakers;
        this.animals = animals;
        this.likes = likes;
    }

    public SocialMediaPost(long id, Set<Caretaker> caretakers, Set<Animal> animals, int likes) {
        this.id = id;
        this.caretakers = caretakers;
        this.animals = animals;
        this.likes = likes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Caretaker> getCaretakers() {
        return caretakers;
    }

    public void setCaretakers(Set<Caretaker> caretakers) {
        this.caretakers = caretakers;
    }

    public Set<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(Set<Animal> animals) {
        this.animals = animals;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
