package cz.cvut.fit.klickjan.tjv_sem.ping;

public class Greeting {
    private String greeting;

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
}
