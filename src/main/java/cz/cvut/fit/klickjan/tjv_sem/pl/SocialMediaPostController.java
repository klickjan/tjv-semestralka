package cz.cvut.fit.klickjan.tjv_sem.pl;

import cz.cvut.fit.klickjan.tjv_sem.bl.SocialMediaPostService;
import cz.cvut.fit.klickjan.tjv_sem.pl.DTOMapper.SocialMediaPostModelAssembler;
import cz.cvut.fit.klickjan.tjv_sem.pl.dto.SocialMediaPostModel;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/social-media-post")
public class SocialMediaPostController {
    private final SocialMediaPostModelAssembler socialMediaPostModelAssembler;
    private final SocialMediaPostService socialMediaPostService;

    public SocialMediaPostController(SocialMediaPostModelAssembler socialMediaPostModelAssembler, SocialMediaPostService socialMediaPostService) {
        this.socialMediaPostModelAssembler = socialMediaPostModelAssembler;
        this.socialMediaPostService = socialMediaPostService;
    }

    @GetMapping(path = "/{id}")
    public SocialMediaPostModel get(@PathVariable("id") long id) {
        return socialMediaPostModelAssembler.toModel(socialMediaPostService.findById(id).get());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SocialMediaPostModel save(@RequestBody SocialMediaPostModel socialMediaPostModel) {
        return socialMediaPostModelAssembler.toModel(
                socialMediaPostService.save(
                        socialMediaPostModelAssembler.fromModel(socialMediaPostModel)
                )
        );
    }

    @PutMapping(path = "/{id}")
    public SocialMediaPostModel update(@PathVariable("id") long id, @RequestBody SocialMediaPostModel socialMediaPostModel) {
        return socialMediaPostModelAssembler.toModel(
                socialMediaPostService.save(
                        socialMediaPostModelAssembler.fromModel(socialMediaPostModel)
                )
        );
    }

    @GetMapping(params = {"animalId", "caretakerId"})
    public List<SocialMediaPostModel> find(@RequestParam("animalId") List<Long> animalIds,
                                           @RequestParam("caretakerId") List<Long> caretakerIds) {
        return socialMediaPostService.findById(animalIds, caretakerIds)
                .stream().map(socialMediaPostModelAssembler::toModel).collect(Collectors.toList());
    }
}
