package cz.cvut.fit.klickjan.tjv_sem.bl;

import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.AnimalRepository;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.CaretakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Service
public class AnimalService {
    private final AnimalRepository animalRepository;

    @Autowired
    public AnimalService(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    public List<Animal> findByCaretakers(List<Long> caretakerIds) {
        return animalRepository.findAnimalByCaretakersIdIn(caretakerIds);
    }

    public List<Animal> findAll() {
        return animalRepository.findAll();
    }

    @Transactional
    public Animal save(Animal animal) {
        if (animal.getWeight() < 0) {
            throw new IllegalArgumentException("Weight must be non-negative");
        }
        if (!animal.getCaretakers().containsAll(animal.getFavouritedCaretakers())) {
            throw new IllegalArgumentException("Favourited caretakers must be current caretakers");
        }
        return animalRepository.save(animal);
    }

    public Optional<Animal> findById(long id) {
        return animalRepository.findById(id);
    }

    public List<Animal> findById(List<Long> ids) {
        return animalRepository.findAllById(ids);
    }

    public void delete(long id) {
        animalRepository.deleteById(id);
    }
}
