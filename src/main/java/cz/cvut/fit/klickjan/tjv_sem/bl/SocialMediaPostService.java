package cz.cvut.fit.klickjan.tjv_sem.bl;

import cz.cvut.fit.klickjan.tjv_sem.dl.dm.SocialMediaPost;
import cz.cvut.fit.klickjan.tjv_sem.dl.repository.SocialMediaPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class SocialMediaPostService {
    private final SocialMediaPostRepository socialMediaPostRepository;

    @Autowired
    public SocialMediaPostService(SocialMediaPostRepository socialMediaPostRepository) {
        this.socialMediaPostRepository = socialMediaPostRepository;
    }

    public Optional<SocialMediaPost> findById(Long id) {
        return socialMediaPostRepository.findById(id);
    }

    public List<SocialMediaPost> findById(List<Long> animalIds, List<Long> caretakerIds) {
        return socialMediaPostRepository.findByAnimalsIdInAndCaretakersIdIn(
                new HashSet<>(animalIds),
                new HashSet<>(caretakerIds)
        );
    }

    public SocialMediaPost save(SocialMediaPost socialMediaPost) {
        return socialMediaPostRepository.save(socialMediaPost);
    }
}
