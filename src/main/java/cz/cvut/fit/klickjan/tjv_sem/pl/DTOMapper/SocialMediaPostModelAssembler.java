package cz.cvut.fit.klickjan.tjv_sem.pl.DTOMapper;

import cz.cvut.fit.klickjan.tjv_sem.bl.AnimalService;
import cz.cvut.fit.klickjan.tjv_sem.bl.CaretakerService;
import cz.cvut.fit.klickjan.tjv_sem.bl.SocialMediaPostService;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.SocialMediaPost;
import cz.cvut.fit.klickjan.tjv_sem.pl.SocialMediaPostController;
import cz.cvut.fit.klickjan.tjv_sem.pl.dto.SocialMediaPostModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

@Component
public class SocialMediaPostModelAssembler implements RepresentationModelAssembler<SocialMediaPost, SocialMediaPostModel> {
    private final CaretakerService caretakerService;
    private final AnimalService animalService;
    private final SocialMediaPostService socialMediaPostService;

    public SocialMediaPostModelAssembler(CaretakerService caretakerService, AnimalService animalService, SocialMediaPostService socialMediaPostService) {
        this.caretakerService = caretakerService;
        this.animalService = animalService;
        this.socialMediaPostService = socialMediaPostService;
    }

    @Override
    public SocialMediaPostModel toModel(SocialMediaPost socialMediaPost) {
        var post = new SocialMediaPostModel(
                socialMediaPost.getId(),
                socialMediaPost.getLikes(),
                socialMediaPost.getCaretakers().stream().map(Caretaker::getId).collect(Collectors.toSet()),
                socialMediaPost.getAnimals().stream().map(Animal::getId).collect(Collectors.toSet())
        );
        post.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(SocialMediaPostController.class).get(socialMediaPost.getId())).withSelfRel());
        return post;
    }

    public SocialMediaPost fromModel(SocialMediaPostModel socialMediaPostModel) {
        SocialMediaPost socialMediaPost;
        if (socialMediaPostModel.getId() > 0) {
            socialMediaPost = socialMediaPostService.findById(socialMediaPostModel.getId()).get();
        } else {
            socialMediaPost = new SocialMediaPost();
        }
        socialMediaPost.setLikes(socialMediaPostModel.getLikes());

        socialMediaPost.setAnimals(new HashSet<>(animalService.findById(new ArrayList<>(socialMediaPostModel.getAnimalIds()))));
        socialMediaPost.setCaretakers(new HashSet<>(caretakerService.findAllById(new ArrayList<>(socialMediaPostModel.getCaretakerIds()))));
        return socialMediaPost;
    }
}
