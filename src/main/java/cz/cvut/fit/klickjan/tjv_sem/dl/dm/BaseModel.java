package cz.cvut.fit.klickjan.tjv_sem.dl.dm;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface BaseModel {
    public long getId();

    public static List<Long> mapToIds(@NotNull Collection<Caretaker> ents) {
        return ents.stream().map(BaseModel::getId).collect(Collectors.toList());
    }
}
