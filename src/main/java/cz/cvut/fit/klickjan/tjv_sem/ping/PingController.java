package cz.cvut.fit.klickjan.tjv_sem.ping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "ping")
public class PingController {
    @Autowired
    private PingService pingService;

    @GetMapping(path = "hello")
    public String hello() {
        return pingService.hello();
    }
}
