package cz.cvut.fit.klickjan.tjv_sem.pl.DTOMapper;

import cz.cvut.fit.klickjan.tjv_sem.bl.CaretakerService;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import cz.cvut.fit.klickjan.tjv_sem.pl.AnimalController;
import cz.cvut.fit.klickjan.tjv_sem.pl.CaretakerController;
import cz.cvut.fit.klickjan.tjv_sem.pl.dto.CaretakerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CaretakerModelAssembler implements RepresentationModelAssembler<Caretaker, CaretakerModel> {
    private CaretakerService caretakerService;

    @Autowired
    public CaretakerModelAssembler(CaretakerService caretakerService) {
        this.caretakerService = caretakerService;
    }

    @Override
    public CaretakerModel toModel(Caretaker caretaker) {
        CaretakerModel caretakerModel = new CaretakerModel(caretaker.getId(), caretaker.getName());
        caretakerModel.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(CaretakerController.class).getOne(caretaker.getId())).withSelfRel());
        caretakerModel.add(
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(AnimalController.class).byCaretakers(List.of(caretaker.getId()))
                ).withRel("taking-care-of")
        );
        return caretakerModel;
    }

    public Caretaker fromModel(CaretakerModel caretakerModel) {
        Caretaker caretaker;
        if (caretakerModel.getId() > 0) {
            caretaker = caretakerService.findById(caretakerModel.getId()).get();
        } else {
            caretaker = new Caretaker();
        }
        caretaker.setName(caretakerModel.getName());
        return caretaker;
    }
}
