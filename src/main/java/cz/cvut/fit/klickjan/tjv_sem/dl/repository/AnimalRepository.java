package cz.cvut.fit.klickjan.tjv_sem.dl.repository;

import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Animal;
import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {
    List<Animal> findByCaretakers(Caretaker caretaker);

    List<Animal> findAnimalByCaretakersIdIn(List<Long> caretakersId);
}
