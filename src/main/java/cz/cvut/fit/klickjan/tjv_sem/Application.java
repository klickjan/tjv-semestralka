package cz.cvut.fit.klickjan.tjv_sem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@SpringBootApplication
@EnableJpaRepositories({"cz.cvut.fit.klickjan.tjv_sem.dl.repository"})
@EntityScan({"cz.cvut.fit.klickjan.tjv_sem.dl.dm"})
@ComponentScan({"cz.cvut.fit.klickjan.tjv_sem.bl"})
@ComponentScan({"cz.cvut.fit.klickjan.tjv_sem.pl"})
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class Application {
    public static void main(String[] args) {
        System.out.println("Henlo fren");
        SpringApplication.run(Application.class, args);
    }
}
