package cz.cvut.fit.klickjan.tjv_sem.dl.repository;

import cz.cvut.fit.klickjan.tjv_sem.dl.dm.Caretaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaretakerRepository  extends JpaRepository<Caretaker, Long> {

}
